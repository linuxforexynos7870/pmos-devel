#!/bin/sh

set -eu

# Messages get printed in bold after the completion of every step.
# Directive: set STEP_TOTAL to the total number of steps.
STEP_COUNT=0
STEP_TOTAL=0

PMB_CFG="$HOME/.config/pmbootstrap.cfg"

PMB_WORK_PATH="$(grep "work = " "$PMB_CFG" | sed "s/work = //")"
PMB_APORTS_PATH="$(grep "aports = " "$PMB_CFG" | sed "s/aports = //")"
PMB_DEVICE_NAME="$(grep "device = " "$PMB_CFG" | sed "s/device = //")"

# Function: pr_step <message>
pr_step()
{
	# Function Arguments
	message="$1"

	STEP_COUNT=$((STEP_COUNT + 1))
	printf "\033[1m[%d/%d] %s\033[0m\n" $STEP_COUNT $STEP_TOTAL "$message"
}

# Function: pr_warn <message>
pr_warn()
{
	# Function Arguments
	message="$1"

	printf "\e[33m\e[1m[*] %s\e[0m\n" "$message"
}

# Function: pr_err <message>
pr_err()
{
	# Function Arguments
	message="$1"

	printf "\e[31m\e[1m[!] %s\e[0m\n" "$message"
	exit 1
}

# Function: find_pkgs <directory>
# Description: Finds the packages in the given directory.
find_pkgs()
{
	# Function Arguments
	directory="$1"

	for pkg in "$directory"/*
	do
		# A package must contain its APKBUILD.
		! [ -f "$pkg/APKBUILD" ] && continue

		basename "$pkg"
	done
}

# Function: find_pkg_type <package-name>
# Description: Finds the type (main, community, or testing) of the package.
find_pkg_type()
{
	# Function Arguments
	package_name="$1"

	for type in "testing" "community" "main"
	do
		# shellcheck disable=SC2010
		if ls "$PMB_APORTS_PATH/device/$type" | grep -q "$package_name"
		then
			echo "$type"
			return
		fi
	done

	# It's possible that the package isn't found anywhere, as the
	# package has never been upstreamed to pmaports. In such cases,
	# fall back to testing.
	echo "testing"
}

# Define the system-on-chip and the device name.
# An empty device name indicates that nothing is mounted.
DEVICE_CHIP="exynos7870"
DEVICE_NAME="$(find_pkgs . | grep "^device-*" | sed "s/^device-//")"

# Path where the out-of-tree pmaports packages are kept. 
OOT_APORTS_PATH=".pmaports/"

# Function: validate_pmb_device
# Description: Validate whether the currently mounted device is the same as the
#              device selected in pmbootstrap or not.
validate_pmb_device()
{
	# Check whether a device is already mounted.
	[ -z "$DEVICE_NAME" ] && pr_err "Mount a device with 'auto.sh mount'."

	if ! [ "$DEVICE_NAME" = "$PMB_DEVICE_NAME" ]
	then
		pr_err "The device chosen in pmbootstrap isn't '$DEVICE_NAME'."
	fi
}

# Function: generate_checksums <package-names>
# Description: Generate checksums for package(s).
generate_checksum()
{
	# Function Arguments
	package_names="$1"

	for pkg in $package_names
	do
		pmbootstrap checksum "$pkg"
	done
}

# Function: command_help
# Description: Prints usage information.
# * Available as a command
command_help()
{
	echo "Usage: auto.sh <command> [<args>]"
	echo 
	echo "The auto.sh script works by hijacking the pmaports packages of"
	echo "pmbootstrap by overriding the upstreamed packages in favour of"
	echo "local ones. Any changes to such packages can be reflected back"
	echo "in local directories."
	echo
	echo "Available auto.sh commannds:"
	echo
	echo 'mount		Mounts the packages of a device'
	echo 'unmount		Unmounts the packages of the mounted device'
	echo 'dumps		Saves any modification in mounted packages'
	echo 'build		Builds all mounted packages'
	echo 'export		Exports flashable images for use'
}

# Function: command_mount <device-name>
# Description: Mounts the device packages in the pmaports tree.
# * Available as a command
command_mount()
{
	# Function Arguments
	device_name="${2:-}"

	STEP_TOTAL=$((STEP_TOTAL + 3))

	# Display the usage terms for invalid arguments.
	[ -z "$device_name" ] && pr_err "Usage: auto.sh $1 <device-name>"

	# Check whether a device is already mounted.
	[ -n "$DEVICE_NAME" ] && pr_err "A device is already mounted."

	pr_step "Checking device packages of pmaports"

	# As with every device, a device package must exist.
	if ! find_pkgs $OOT_APORTS_PATH | grep -q "^device-$device_name$"
	then
		pr_err "This device does not have any packages."
	fi

	pkgs_common="$(find_pkgs $OOT_APORTS_PATH | grep "^.*-$DEVICE_CHIP$")"
	pkgs_device="$(find_pkgs $OOT_APORTS_PATH | grep "^.*-$device_name$")"

	# Check whether the packages have been modified locally.
	# Make sure that the script doesn't overwrite the local changes.
	# It's also possible that another auto.sh somewhere has loaded the
	# out-of-tree packages.
	cd "$PMB_APORTS_PATH"
	for pkg in $pkgs_common $pkgs_device
	do
		type="$(find_pkg_type "$pkg")"

		if [ -n "$(git status "device/$type/$pkg" --porcelain)" ]
		then
			pr_err "Package $pkg is unclean"
		fi
	done
	cd - > /dev/null

	pr_step "Replacing device packages in pmaports"

	# Delete the packages in pmaports, and copy the out-of-tree packages
	# over to their destinations.
	for pkg in $pkgs_common $pkgs_device
	do
		type="$(find_pkg_type "$pkg")"

		rm -r "$PMB_APORTS_PATH/device/$type/$pkg" > /dev/null 2>&1 || true
		cp -r "$OOT_APORTS_PATH/$pkg" "$PMB_APORTS_PATH/device/$type"
	done

	pr_step "Creating symlinks to device package directories"

	# Create symlinks for the mounted packages.
	for pkg in $pkgs_common $pkgs_device
	do
		type="$(find_pkg_type "$pkg")"

		ln -fs "$PMB_APORTS_PATH/device/$type/$pkg" .
	done

	if ! [ "$device_name" = "$PMB_DEVICE_NAME" ]
	then
		pr_warn "Run 'pmbootstrap init' and select your device."
	fi
}

# Function: command_dump
# Description: Dumps the device packages from the pmaports tree.
# * Available as a command
command_dump()
{
	STEP_TOTAL=$((STEP_TOTAL + 3))

	validate_pmb_device

	pkgs="$(find_pkgs .)"

	pr_step "Generating checksums for device packages"

	generate_checksum "$pkgs"

	pr_step "Purging existing device packages"

	# Delete the out-of-tree device packages.
	# They will be replaced with new packages from the pmaports tree.
	for pkg in $pkgs
	do
		# shellcheck disable=SC2115
		rm -r "$OOT_APORTS_PATH/$pkg"
	done

	pr_step "Copying device packages to the repository"

	# Copy the device packages from pmaports tree to the out-of-tree package
	# directory.
	for pkg in $pkgs
	do
		type="$(find_pkg_type "$pkg")"

		cp -r "$PMB_APORTS_PATH/device/$type/$pkg" "$OOT_APORTS_PATH/"
	done
}

# Function: command_unmount
# Description: Unmounts the device packages from the pmaports tree.
# * Available as a command
command_unmount()
{
	STEP_TOTAL=$((STEP_TOTAL + 2))

	command_dump

	pkgs="$(find_pkgs .)"

	pr_step "Deleting symlinks to device package directories"

	# Remove the package symlinks.
	# shellcheck disable=SC2086
	rm $pkgs

	pr_step "Purging existing device packages from pmaports"

	# Restore the original unmodified versions of packages using git.
	cd "$PMB_APORTS_PATH"
	for pkg in $pkgs
	do
		type="$(find_pkg_type "$pkg")"

		rm -r "device/$type/$pkg"
		git restore "device/$type/$pkg" 2> /dev/null || true
	done
	cd - > /dev/null
}

# Function: command_build [packages]
# Description: Build the device packages.
# * Available as a command
command_build()
{
	# Function Arguments
	packages="${2:-}"

	validate_pmb_device

	pkgs="$(find_pkgs . | grep "$packages" || true)"

	# If no packages are available, display an error.
	[ -z "$pkgs" ] && pr_err "Zero packages selected."

	STEP_TOTAL=$((STEP_TOTAL + 1 + $(echo "$pkgs" | wc -w)))

	pr_step "Generating checksums for device packages"

	generate_checksum "$pkgs"

	# Force build the selected packages.
	for pkg in $pkgs
	do
		pr_step "Building package - $pkg"
		pmbootstrap build --force "$pkg"
	done
}

# Function: command_export
# Description: Export the flashable images.
# * Available as a command
# TODO: Add option for recovery builds.
command_export()
{
	STEP_TOTAL=$((STEP_TOTAL + 1))

	validate_pmb_device

	pr_step "Exporting flashable images"

	pmbootstrap export --no-install

	cp "/tmp/postmarketOS-export/boot.img" .
	cp "/tmp/postmarketOS-export/$DEVICE_NAME.img" .
}

# Run the command.
# For invalid commands, execute the help command instead.
if command -V command_"${1:-}" > /dev/null 2>&1
then
	command_"${1:-}" "$@"
else
	command_help
fi
