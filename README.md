# PostmarketOS development - Exynos7870

### Currently supported devices:
- <a href="https://wiki.postmarketos.org/wiki/Samsung_Galaxy_J7_Prime_(samsung-on7xelte)">Samsung Galaxy J7 Prime (samsung-on7xelte)
- <a href="https://wiki.postmarketos.org/wiki/Samsung_Galaxy_A2_Core_(samsung-a2corelte)">Samsung Galaxy A2 Core (samsung-a2corelte)
